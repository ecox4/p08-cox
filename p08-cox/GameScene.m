//
//  GameScene.m
//  p08-cox
//
//  Created by Em on 5/9/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "GameScene.h"
#import "Universe.h"

@interface GameScene() <SKPhysicsContactDelegate>

    @property BOOL contentCreated;
    @property BOOL gameOver;
    @property (atomic) int throws;
    @property (atomic) int pScore;

@end

@implementation GameScene{
    SKSpriteNode *bg;
    SKSpriteNode *play;
    SKSpriteNode *cup;
    SKLabelNode *score;
    SKSpriteNode *goScreen;
    SKLabelNode *hiScore;
}

-(void)didMoveToView:(SKView *)view{
    if(!self.contentCreated){
        [self createSceneContents];
        self.contentCreated = YES;
        self.gameOver = NO;
        self.throws = 0;
        self.pScore =0;
        self.physicsWorld.contactDelegate = self;
    }
}

-(void)createSceneContents{
    self.scaleMode = SKSceneScaleModeFill;
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.backgroundColor = [SKColor whiteColor];
    [self addChild:[self setBg]];
    [self addChild:[self makeCup]];
    [self addChild:[self makePlayer]];
    [self addChild:[self setScore]];
}
-(void)update:(NSTimeInterval)currentTime{
    //frame-by-frame check
    if(self.gameOver){
        [play removeFromParent];
        [cup removeFromParent];
        [score removeFromParent];
        [self addChild:[self doGameOver]];
        [self addChild:[self showHighScore]];
        self.throws = 0;
        self.gameOver = NO;
    }
    if(self.throws == 7){
        self.gameOver = YES;
    }
    if((NSLocationInRange(play.position.x, NSMakeRange(CGRectGetMidX(self.frame)-100, ((CGRectGetMidX(self.frame)+100)-(CGRectGetMidX(self.frame)-100))))) && (NSLocationInRange(play.position.y, NSMakeRange(CGRectGetMidY(self.frame)-20, ((CGRectGetMidY(self.frame)+20)-(CGRectGetMidY(self.frame)-20)))))){
        //ball is in trashcan
        [play removeFromParent];
        [self sunkPlay];
    }
    if((play.physicsBody.velocity.dx < 1)&&(play.physicsBody.velocity.dy < 1)){
        CGVector vector = CGVectorMake(0, 0);
        [play.physicsBody applyImpulse:vector];
    }
    score.text = [NSString stringWithFormat:@"%d",self.pScore];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];

    SKNode *node = [self nodeAtPoint:location];
    if([node.name isEqualToString:@"gameover"]){
        [goScreen removeFromParent];
        [self addChild:[self makePlayer]];
        [self addChild:[self makeCup]];
        [self addChild:[self setScore]];
        [hiScore removeFromParent];
        self.pScore = 0;
    }
    if(!goScreen.parent){
        self.throws++;
    }
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    if((play.physicsBody.velocity.dx == 0)&&(play.physicsBody.velocity.dy ==0)){
        CGVector vector = CGVectorMake((location.x - play.position.x)*-7, (location.y - play.position.y)*-7);
        [play.physicsBody applyImpulse:vector];
    }
    self.pScore += 5;
}
-(void)sunkPlay{
    [self addChild:[self makePlayer]];
    self.pScore += 100;
    self.throws = 0;
}
-(SKSpriteNode *)setBg{
    bg = [SKSpriteNode spriteNodeWithImageNamed:@"bg.jpeg"];
    bg.size = self.size;
    bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    bg.name = @"bg";
    return bg;
}
-(SKSpriteNode *)makePlayer{
    play = [SKSpriteNode spriteNodeWithImageNamed:@"trash.png"];
    play.size = CGSizeMake(50, 50);
    play.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:play.frame.size.width/2];
    play.position = CGPointMake(CGRectGetMidX(self.frame), 200);
    play.physicsBody.affectedByGravity = NO;
    play.physicsBody.dynamic = YES;
    play.physicsBody.restitution = 0.1;
    play.physicsBody.linearDamping = 0.5;
    play.physicsBody.angularDamping = 0.7;
    play.name = @"play";
    return play;
}
-(SKSpriteNode *)makeCup{
    cup = [SKSpriteNode spriteNodeWithImageNamed:@"trashcan.png"];
    cup.size = CGSizeMake(100, 100);
    cup.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    cup.physicsBody =[SKPhysicsBody bodyWithEdgeLoopFromRect:cup.frame];
    cup.physicsBody.affectedByGravity = NO;
    cup.name = @"cup";
    return cup;
}
-(SKLabelNode *)setScore{
    score = [SKLabelNode labelNodeWithText:[NSString stringWithFormat:@"%d",self.pScore]];
    score.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)-100);
    score.fontName = @"Helvetica";
    score.fontSize = 28;
    score.fontColor = [SKColor blackColor];
    score.name = @"score";
    return score;
}
-(SKSpriteNode *)doGameOver{
    [self setBest];
    goScreen = [SKSpriteNode spriteNodeWithImageNamed:@"gameover.png"];
    goScreen.size = CGSizeMake(600, 900);
    goScreen.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    goScreen.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:goScreen.frame];
    goScreen.name = @"gameover";
    return goScreen;
}
-(SKLabelNode *)showHighScore{
    hiScore = [SKLabelNode labelNodeWithText:[NSString stringWithFormat:@"Best: %d",[[Universe sharedInstance]counter]]];
    hiScore.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-100);
    hiScore.fontName = @"Helvetica";
    hiScore.fontSize = 60;
    hiScore.fontColor = [SKColor whiteColor];
    return hiScore;
}
-(void)setBest{
    if(self.pScore > [[Universe sharedInstance]counter]){
        [[Universe sharedInstance]setCounter:self.pScore];
    }
}
@end
