//
//  ViewController.m
//  p08-cox
//
//  Created by Em on 4/17/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "ViewController.h"
#import "GameScene.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SKView *spriteView = (SKView *)self.view;
    spriteView.showsFPS = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    GameScene *game = [[GameScene alloc]initWithSize:CGSizeMake(800, 1200)];
    SKView *spriteView = (SKView *)self.view;
    [spriteView presentScene:game];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
