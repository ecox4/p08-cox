//
//  Universe.m
//  p08-cox
//
//  Created by Em on 5/10/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "Universe.h"

@implementation Universe
@synthesize counter;

static Universe *singleton = nil;
-(id)init{
    if(singleton){
        return singleton;
    }
    self = [super init];
    if(self){
        singleton = self;
    }
    return self;
}
+(Universe *)sharedInstance{
    if(singleton){
        return singleton;
    }
    return [[Universe alloc]init];
}
-(void)saveState{
    NSArray *dirs = [[NSFileManager defaultManager]URLsForDirectory:NSApplicationDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager]createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *url = [NSURL URLWithString:@"score.archive" relativeToURL:[dirs objectAtIndex:0]];
    
    NSMutableData *data = [[NSMutableData alloc]init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
    [archiver encodeInt:counter forKey:@"counter"];
    [archiver finishEncoding];
    [data writeToURL:url atomically:YES];
    NSLog(@"Save score %d",counter);
}
-(void)loadState{
    NSArray *dirs = [[NSFileManager defaultManager]URLsForDirectory:NSApplicationDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager]createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *url = [NSURL URLWithString:@"score.archive" relativeToURL:[dirs objectAtIndex:0]];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    if(!data){
        return;
    }
    
    NSKeyedUnarchiver *unarchiver;
    unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    counter = [unarchiver decodeIntForKey:@"counter"];
    NSLog(@"Load score %d",counter);
}

@end
